# Vitrify Theme
Vitrify is a theme for discord utilising **BetterDiscord** theme feature with **Glasstron/Glasscord**'s feature in mind\
This means that the theme wouldn't look like the ones in the preview without **Glasstron/Glasscord**, however it is possible to set your own background image or color without the use of **Glasstron/Glasscord**!

tested on Windows 10, with **BetterDiscord** and **Glasscord**

## Preview
<img src=https://lifegrain.gitlab.io/Vitrify/preview/Vitrify%20screenshot%201.jpg width=49% alt="Screenshot with only desktop wallpaper">
<img src=https://lifegrain.gitlab.io/Vitrify/preview/Vitrify%20screenshot%202.jpg width=49% alt="Screenshot with something behind">

<img src=https://lifegrain.gitlab.io/Vitrify/preview/Sliding%20Members%20List.gif height=300px alt="Demonstration of Sliding Members List">
<img src=https://lifegrain.gitlab.io/Vitrify/preview/Settings%20Icon%201.jpg height=300px alt="Preview of Settings Icon 1">
<img src=https://lifegrain.gitlab.io/Vitrify/preview/Settings%20Icon%202.jpg height=300px alt="Preview of Settings Icon 2">
<img src=https://lifegrain.gitlab.io/Vitrify/preview/Pin%20Add%20server%20and%20Explore%20Button.gif height=300px alt="Demonstration of Sticky Add Server and Explore Button">
<img src=https://lifegrain.gitlab.io/Vitrify/preview/Member%20list%20role%20gradient.gif height=300px alt="Member List Role Gradient">
<img src=https://lifegrain.gitlab.io/Vitrify/preview/Member%20list%20role%20gradient%20+%20Sliding%20Member%20list.gif height=300px alt="Member List Role Gradient in combination of Sliding Member List">
<br>

<img src=https://lifegrain.gitlab.io/Vitrify/preview/Channel%20List%20Redesign.gif height=100px alt="Channel List Redesign">
<img src=https://lifegrain.gitlab.io/Vitrify/preview/Compact%20Status%20Picker.gif height=100px alt="Compact Status Picker">


## Features
- Set Custom Background Image/Color
- Supports Glasscord/Glasstron composition effects (Blurry see-through windows)
### Enable/Disable import, as your taste desires
#### Sliding Members List
Members list will shrink when you do not hover over it, and grows when you hover over it

#### Embed Redesign by Alexis WyvernZu (modified)
Fancy subtle gradient look as your embed background

#### Settings Icon Menu by DevilBro
Icons for settings categories

#### Profile Connection Borders by Strencher
connections in profile modal has colored Borders

#### Pin Home Button by Doggybootsy
Home Button stays even if you scroll your server(guild) list

#### Pinned Add Server and Explore Button
Add Server and Explore button stays at the bottom, even if you scroll your server(guild) list

#### Channel List Redesign
Fancy new look for your Channel List! Category is centered, selected channel gets a fancy gradient background that pulses, hover over a channel and enjoy your pulsing glow, and finally unread channels indicator is now just a clean line instead of round-ish

#### Compact Status Picker by CorellanStoma (modified)
Fancy Compact Status Picker, along with animation!

## Dependencies
[**BetterDiscord**](https://betterdiscord.app)

### not required (for the blurred see-through effect)
[**Glasscord**](https://github.com/AryToNeX/Glasscord/releases)\
Instruction on installion/using Glasscord can be found [here](https://github.com/AryToNeX/Glasscord/blob/master/README.md)\
However it is no longer in active development, so **use it at your own risk!**

#### Without Glasscord/Glasstron?
it is possible, just replace this line in your copy of Vitrify.theme.css
```css
	/*--background-image-url: url('https://cdn.discordapp.com/attachments/327481709006159881/886511299004092427/Ok.png');
	--background-filters: blur(15px);*/
```
into this
```css
    --background-image-url: url('https://cdn.discordapp.com/attachments/327481709006159881/886511299004092427/Ok.png');
	--background-filters: blur(15px);
```
**i'd suggest you replace that url into an image you prefer**

## Download
[Download](https://lifegrain.gitlab.io/Vitrify/Vitrify.theme.css)

right click, save as
